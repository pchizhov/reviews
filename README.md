## 1. Ссылка на проект

Весь проект можно найти по [ссылке](https://github.com/pchizhov/itmo-practice)

## 2. Структура проекта
Проект имеет следующуюю структуру:
1. Пакет **markup**, включающий в себя файлы: 
* **tripadvisor.py** - необходим для сбора отзывов с сайта [TripAdvisor](https://www.tripadvisor.co.uk);
* **manual_markup.py** - веб-интерфейс для разметки отзывов, реализованный с помощью bottle;
* **reviews_temlate.py** - шаблон таблицы;
2. Пакет **train**, включающий в себя файлы: 
* **create_csv.py** - создает файл "reviews.csv" с матрицей слов в размеченных отзывах;
* **train_model.py** - тренирует модель на основе размеченных отзывов;
3. Пакет **storage_files**:
* **lr_model.sav** - сохраненная модель;
* **review.db** - база данных sqlite с таблицей с отзывами;
* **reviews.csv** - матрица слов в размеченных отзывах;
* **vocabulary.json** - файл со всеми словами, на основе которых построена модель;
4. Файл **db.py** - создание и работа с базой данных;
5. Файл **classifier.py** - классификатор отзывов на основе модели;
6. Файл **main.py** - запуск веб-интерфейса.

Стоит отметить, что в директории проекта необходимо создать файл **config.py**, содержащий пути к вспомогательным файлам.
```python
MODEL_PATH = ""
VOCABULARY_PATH = ""
CSV_PATH = ""
DB_PATH = ""
CATEGORY_WORDS = ["food", "service", "ambiance", "price", "location"]
```

## 3. Сбор отзывов
В проекте реализован сбор отзывов с сайта [TripAdvisor](https://www.tripadvisor.co.uk), 
осуществляющийся методами из файла **tripadvisor.py**.

Для загрузки страницы использована библиотека `webdriver` от selenium.
```python
from selenium import webdriver
driver = webdriver.Safari()
```
Я использовал браузер Safari, в случае необходимости можно заменить название аналогичным образом.
Далее цикл проходит по страницам отзывов о заданном ресторане, по умолчанию на одной странице показываются
10 отзывов, для перехода к следующим используется кнопка `next`. В методе установлена верхняя граница
количества нажатий на `next` на странице ресторана. Нажатие на эту кнопку (если она есть) происходит с помощью
метода `find_elements_by_class_name`.
```python
reviews_list = []
for _ in range(5):
    driver.get(url)
    print("Collecting data from page: {}\n".format(url))
    reviews_list.extend(process_page(driver))
    driver.get(url)
    next_page = driver.find_elements_by_class_name("nav.next.taLnk.ui_button.primary")
    time.sleep(2)
    if next_page:
        url = next_page[0].get_attribute("href")
    else:
        break
```
В цикле выше используется функция `process_page(web_driver)`.
```python
def process_page(web_driver):
    click_more(web_driver)
    soup = BeautifulSoup(web_driver.page_source, "html.parser")
    return extract_reviews_data(soup)
```
Для разворачивания длинных отзывов используется функция `click_more(web_driver)`
```python
def click_more(web_driver):
    more_spans = web_driver.find_elements_by_class_name("taLnk.ulBlueLinks")
    if more_spans:
        more_spans[0].click()
        time.sleep(2)
```
Для парсинга используется `BeautifulSoup`, этому соответствует функция `extract_reviews_data(parser)`
```python
def extract_reviews_data(parser):
    for div in parser.findAll("div", {"class": "mgrRspnInline"}):
        div.decompose()  # remove managers' responses
    texts = [remove_ns(r.find("p").getText()) for r in
             parser.findAll("div", {"class": "prw_rup prw_reviews_text_summary_hsx"})
             if len(r.find("p").getText()) < 400]
    return texts
```
Функция `remove_ns(text)` с помощью регулярного выражения заменяет символы табуляции на пробелы.
Основная функция `get_reviews(url)` возвращает список текстов отзывов короче 400 знаков, в которых символы
перехода на новую строку заменены на пробелы, чтобы не сливать воедино слова окончания и начала двух строк.

## 4. Подключение базы данных
В работе используется база данных `sqlite` и библиотека `sqlalchemy` для работы с ней. В базе всего одна таблица **review**:
```python
class Review(Base):
    __tablename__ = "review"
    id = Column(Integer, primary_key=True)
    text = Column(String)
    food = Column(Integer)
    service = Column(Integer)
    ambiance = Column(Integer)
    price = Column(Integer)
    location = Column(Integer)
    marked = Column(Integer)
```
Для категорий в базе используется тип данных Integer, поля заполняются значениями 0 при отсутствии категории и 1 при наличии.
Поле **marked** по умолчанию равно 0, при подтверждении правильности размеченных категорий становится равным 1.

Реализованы функции заполнения и выборки отзывов.
Для заполнения базы используется следующая функция:
```python
def fill(review, food=0, service=0, ambiance=0, price=0, location=0, marked=0):
    s = session()
    rows = s.query(Review).filter(Review.text == review).all()
    if rows:
        row = rows[0]
        row.food = food
        row.service = service
        row.ambiance = ambiance
        row.price = price
        row.location = location
        row.marked = marked
    else:
        review = Review(text=review,
                        food=food,
                        service=service,
                        ambiance=ambiance,
                        price=price,
                        location=location,
                        marked=marked)
        s.add(review)
    s.commit()
```
Эта функция универсально используется для добавления новых отзывов и обновления категорий для старых.

Для выборки отзывов используется следующая функция:
```python
def load_reviews(labeled=True):
    s = session()
    rows = s.query(Review).filter(Review.marked == int(labeled)).all()
    return [make_dict(r) for r in rows]
```
Функция возвращает список словарей размеченных или неразмеченных отзывов, в зависимости от значения параметра `labaled`.

## 5. Разметка отзывов вручную
Для разметки отзывов необходимо прежде всего запустить файл **main.py**, запускающий веб-интерфейс.
```python
Bottle v0.12.16 server starting up (using WSGIRefServer())...
Listening on http://localhost:8080/
Hit Ctrl-C to quit.
```
Веб страница состоит из таблицы **еще не размченных** отзывов, для каждого из которых существую ячейки с текущими значениями категорий из базы данных.
![](interface.png)
При нажатии на эти значения происходит смена значения на противополножное, это реализовано с помощью следующего метода.
```python
@route("/add_cat/")
def add_cat():
    s = session()
    cat = str(request.query.cat)
    row_id = request.query.id
    row = s.query(Review).filter(Review.id == row_id).one()
    if cat == "food":
        row.food += 1
        row.food %= 2
    elif cat == "service":
        row.service += 1
        row.service %= 2
    elif cat == "ambiance":
        row.ambiance += 1
        row.ambiance %= 2
    elif cat == "price":
        row.price += 1
        row.price %= 2
    elif cat == "location":
        row.location += 1
        row.location %= 2
    s.commit()
    redirect("/reviews")
```
При нажатии на кпопку `submit` происходит смена значения поля **marked** на 1 и строка отзыва исчезает.
В случае, если все отзывы размечены, можно нажать на кнопку `Wanna more reviews!` внизу страницы, тогда произойдет переход
к простой форме, в которую нужно будет вставить ссылку на новый ресторан, отзывы к которум нужно собрать, и по нажатии кнопки `Go!`
сбор отзывов начнется.

## 6. Обучение
Перед обучением модели необходимо обработать накопленные отзывы и получить из них чистый текст.
Механизм препроцессинга устроен в основном так же, как и в [исходном классификаторе](https://gitlab.com/itmo-dev/data-analysis/yelpdatasetreviewscategories/tree/artem-dev).
Изменения коснулись механизма удаления пунктуации из текста, знаки препинания заменяются пробелом, из-за того 
что многие авторы отзывов не следят за печатным текстом и не ставят пробелы там, где нужно.
```python
def punctuate(text):
    no_punctuation = re.compile("[.;:\-!\'’?,\"()&]")
    text = no_punctuation.sub(" ", text.lower())
    text = re.sub(r"\s+", " ", text)
    return text
```
Из-за наличия чисел в тексте в отзывах должны оставаться только слова. Несмотря на то что числа могут намекать
на цену, их совпадение в разных отзывах очень маловероятно.
```python
alphabet = set(list("abcdefghijklmnopqrstuvwxyz"))

def check_token(t):
    return not t.is_stop and len(t) > 1 and set(list(str(t))).issubset(alphabet)
```
После препроцессинга используется `CountVectorizer` для создания матрицы вхождения слов в отзывы и категорий для этих отзывов.
```python
vec = CountVectorizer()
x = vec.fit_transform(clean_reviews)
```
```python
print(x.todense())
```
```python
[[0 0 0 ... 0 0 0]
 [0 0 0 ... 0 0 0]
 [0 0 0 ... 0 0 0]
 ...
 [0 0 0 ... 0 0 0]
 [0 0 0 ... 0 0 0]
 [0 0 0 ... 0 0 0]]
```
После сопоставления матрицы и словаря создается (перезаписывается) файл **reviews.csv**.

В файле **train_model.py** происходит обучение классификатора. Запустить обучение можно из основной страницы веб-интерфейса,
нажав на кнопку `Train model!`. 
```python
@route("/train")
def train():
    f1_score = train_model()
    print("Model successfully trained with score: {}\n".format(f1_score))
    redirect("/reviews")
```
При этом в системный вывод будет напечатано сообщение с `F1 score` для обученной модели.

## 7. Классификация
В проекте используется технология Semi-Supervised Learning. Файл **classifier.py** содержит методы для классификации отзывов.
С помощью библиотеки `pickle` модель загружается.
```python
with open(config.MODEL_PATH, 'rb') as lr_f:
    model_loaded = pickle.load(lr_f)
return model_loaded
```
Из базы данных выбираются еще не размеченныет отзывы.
```python
reviews_texts = [r["text"] for r in load_reviews(labeled=False)]
```
Главной особенностью является функция `get_categories()`.
```python
def get_categories(model, x_data, row_data):
    pred = model.predict(x_data)
    proba = model.predict_proba(x_data)
    cnt = [0, 0]
    for i, p in enumerate(pred):
        indices = np.where(p)
        clear = True
        for e in proba[i]:
            if 0.050 < e < 0.800:
                clear = False
                break
        cats = [0, 0, 0, 0, 0]
        for idx in indices[0]:
            cats[int(idx)] = 1
        fill(row_data[i], cats[0], cats[1], cats[2], cats[3], cats[4], int(clear))
        cnt[int(clear)] += 1
    print("Reviews successfully classified with {} clear and {} confusing reviews\n".format(cnt[1], cnt[0]))
```
Отзывы обновляют свои категории в базе данных на основе предсказанных классификатором,
и, если классификатор уверен в категориях, о чем свидетельствует вероятность, помечает отзыв в базе данных как размеченный.

Классифицировать неразмеченные отзывы можно точно так же нажатием кнопки `Predict labels!` внизу главной страницы интерфейса.
```predict
@route("/predict")
def predict():
    classify()
    redirect("/reviews")
```

## 8. Заключение
Разработанная программа позволяет собирать и размечать вручную категории для отзывов с [TripAdvisor](https://www.tripadvisor.co.uk),
обучать модель на размеченных отзывах, а также классифицировать новые отзывы на основе модели, используя
механизм Semi-Supervised Learning для разметки отзывов, в которых модель уверена, и предложения категорий
для ручной разметки в тех отзывах, в которых модель не уверена.

